Given the following data, provide the answer of the ff:

a. List the books authored by Marjorie Green.
	- The Busy Executive's Database Guide
	- You Can Combat Computer Stress!
a. List the books authored by Michael O'Leary.
	- Cooking with Computers
	- TC7777 not found
c. Write the author/s of "The Busy Executives Database Guide".
	- Marjorie Green
	- Abraham Bennet
d. Identify the publisher of "But Is is User Friendly?".
	- Algodata Infosystems
e. List the books published by Algodata Infosystems.
	- The Busy Executive's Database Guide
	- Cooking with Computers
	- Straight Talk About Computers
	- But Is Is User Friendly?
	- Secrets of Silicon Valley
	- Net Etiquette
'
CREATE SQL SYNTAX AND QUERIES TO CREATE A DATABSE BASED ON THE ERD:

Creating the database:
	- CREATE DATABASE blog_db;
Selecting the database:
	- USE blog_db;
Adding the tables:
	Creating table for USERS:
   		=  CREATE TABLE users (id INT NOT NULL AUTO_INCREMENT, email VARCHAR(100) NOT NULL, password VARCHAR(300) NOT NULL, datetime_created DATETIME NOT NULL, PRIMARY KEY (id));
	Creating table for POSTS:
		=  CREATE TABLE posts (id INT NOT NULL AUTO_INCREMENT, author_id INT NOT NULL, title VARCHAR(500) NOT NULL, content VARCHAR(5000) NOT NULL, datetime_posted DATETIME NOT NULL, PRIMARY KEY (id), CONSTRAINT fk_posts_author_id FOREIGN KEY(author_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT);
  	Creating table for POST_LIKES:
  		=  CREATE TABLE post_likes (id INT NOT NULL AUTO_INCREMENT, post_id INT NOT NULL, user_id INT NOT NULL, datetime_liked DATETIME NOT NULL, PRIMARY KEY (id), CONSTRAINT fk_post_likes_post_id FOREIGN KEY(post_id) REFERENCES posts(id) ON UPDATE CASCADE ON DELETE RESTRICT, CONSTRAINT fk_post_likes_user_id FOREIGN KEY(user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT);
  	Creating table for POST_COMMENTS:
  		=  CREATE TABLE post_comments (id INT NOT NULL AUTO_INCREMENT, post_id INT NOT NULL, user_id INT NOT NULL, content VARCHAR(5000) NOT NULL, datetime_commented DATETIME NOT NULL, PRIMARY KEY(id), CONSTRAINT fk_post_comments_post_id FOREIGN KEY(post_id) REFERENCES posts(id) ON UPDATE CASCADE ON DELETE RESTRICT, CONSTRAINT fk_post_comments_user_id FOREIGN KEY(user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT);